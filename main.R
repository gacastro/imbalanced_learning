# benchmarking oversampling implementations
# Author: Gauthier CASTRO
# Dec 2018

source('code/smote.R')
source('code/BL_SMOTE.R')
source('code/adasyn.R')

library(caret)
library(glmnet)
library(e1071)

# dataset loader to numeric
load_numeric <- function(filename){
  df <- RKEEL::read.keel(filename)
  num_data <- data.frame(data.matrix(df))
  numeric_columns <- sapply(num_data,function(x){mean(as.numeric(is.na(x)))<0.5})
  final_data <- num_data[,numeric_columns]
  final_data[,names(final_data)!='Class'] <- scale(final_data[,names(final_data)!='Class'])
  return(final_data)
}

# smote wrapper to return oversampled dataset
load_smote <- function(data, N = 300, k = 5){
  res <- rbind(data, setNames(data.frame(SMOTE(data, ncol(data), N = 300, k = 5)), nm = names(data)))
  return(res)
}

# berderline smote wrapper to return oversampled dataset
load_blsmote <- function(data, K = 5, C = 3){
  res <- rbind(data, setNames(data.frame(BL_SMOTE(data[,-ncol(data)], data[,ncol(data)], K = K, C = C)), nm=names(data)))
  return(res)
}

# ovsasyn wrapper to return oversampled dataset
load_adasyn <- function(data, k = 5, beta = 1){
  res <- rbind(data, setNames(data.frame(adasyn(data, ncol(data), k, beta)), nm=names(data)))
  return(res)
}

# ground truth
aba <- load_numeric('data/abalone19.dat')
pim <- load_numeric('data/pima.dat')
seg <- load_numeric('data/segment0.dat')
veh <- load_numeric('data/vehicle0.dat')
wis <- load_numeric('data/wisconsin.dat')

aba_smote <- load_smote(aba, N = 300, k = 5)
pim_smote <- load_smote(pim, N = 300, k = 5)
seg_smote <- load_smote(seg, N = 300, k = 5)
veh_smote <- load_smote(veh, N = 300, k = 5)
wis_smote <- load_smote(wis, N = 300, k = 5)

aba_BLSMOTE <- load_blsmote(data, K = 5, C = 3)
pim_BLSMOTE <- load_blsmote(data, K = 5, C = 3)
seg_BLSMOTE <- load_blsmote(data, K = 5, C = 3)
veh_BLSMOTE <- load_blsmote(data, K = 5, C = 3)
wis_BLSMOTE <- load_blsmote(data, K = 5, C = 3)

aba_adasyn <- load_adasyn(aba, k = 5, beta = 1)
pim_adasyn <- load_adasyn(pim, k = 5, beta = 1)
seg_adasyn <- load_adasyn(seg, k = 5, beta = 1)
veh_adasyn <- load_adasyn(veh, k = 5, beta = 1)
wis_adasyn <- load_adasyn(wis, k = 5, beta = 1)


# common statistics about the data
data <- pim
fun <- load_adasyn
summary()
learn <- function(data, fun = NULL){
  # split training and test set
  validation_index <- createDataPartition(data$Class, p=0.70, list=FALSE)
  test <- data[-validation_index,]
  train <- data[validation_index,]
  rownames(train) <- 1:nrow(train)
  
  # oversampled
  if (is.null(fun)) {
    ovsa <- train
  } else {
    ovsa <- fun(train)
  }
  
  ovsa$Class <- as.factor(ovsa$Class)
  
  Xi <- colnames(ovsa)[-ncol(train)]
  yi <- colnames(ovsa)[ncol(train)]
  
  ## training
  # penalized logistic regression
  logi_reg <- cv.glmnet(x=as.matrix(ovsa[,Xi]), 
                    y=as.factor(ovsa$Class),
                    alpha = 0.5,
                    family = 'binomial')
  
  plot(logi_reg)
  
  ## predict
  pred <- predict(logi_reg, as.matrix(ovsa[,Xi]), type='class',s=logi_reg$lambda.min)
  confusion <- confusionMatrix(as.factor(pred), as.factor(ovsa$Class))
  print('train set')
  print(confusion)
  
  pred_test <- predict(logi_reg, as.matrix(test[,Xi]), type='class',s=logi_reg$lambda.min)
  confusion <- confusionMatrix(as.factor(pred_test), as.factor(test$Class))
  print('test set')
  print(confusion)
  
  library(pROC)
  roc.c <- roc(as.numeric(test$Class), as.numeric(pred_test))
  plot(roc.c)
  
  # SVM
  #aba_svm <- svm((Class) ~ ., data=train, kernel="linear", type='C-classification')
  
  # confusion matrix
  #pred <- predict(aba_svm, test[,Xi])
  #table(pred,test[,yi])
}

learn(pim)




# boxplots
par(mfrow=c(2,ncol(train)/2))
for(i in 1:ncol(train)) {
  boxplot(train[,Xi][,i], main=names(train)[i])
}

# scatter plots, vss.
obj <- featurePlot(train[,Xi], train[,yi], plot="pairs")
print(obj)
dev.off()

